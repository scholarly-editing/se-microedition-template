import type { Actions, CreatePagesArgs, GatsbyNode, Reporter } from "gatsby";
import path from "path";


function slugify(text:string) {
  return text.toLowerCase()
    .replace(/<[^>]+>/g, '') // remove html tags
    .replace(/ /g,'-') // spaces become -
    .replace(/-+/g, '-') // no repeated -
    .replace(/[^\w-]+/g,'') // remove all non word or - characters. TODO: this only saves ASCII characters!
}

interface IMakePages {
  createPage: Actions["createPage"]
  reporter: Reporter
  graphql: CreatePagesArgs["graphql"]
}

export const createPages: GatsbyNode["createPages"] = async ({ actions, graphql, reporter }) => {
  const utils: IMakePages = {createPage: actions.createPage, reporter, graphql}
  await makeIntroduction(utils)
  await makeCeteiceanPages(utils)
}

async function makeCeteiceanPages({createPage, reporter, graphql}: IMakePages): Promise<void> {
  const component = path.resolve(`./src/gatsby-theme-ceteicean/components/Ceteicean.tsx`)

  const result = await graphql(`
  query TeiPages {
    allCetei {
      nodes {
        prefixed
        elements
        parent {
          ... on File {
            name
            ext
          }
        }
      }
    }
    site {
      siteMetadata {
        htmlTitle
        issue {
          short
        }
      }
    }
  }
`)
  if (result.errors) {
    reporter.panicOnBuild(`Error while running GraphQL query.`)
    return
  }

  const data = result.data as Queries.TeiPagesQuery

  for (const node of data.allCetei.nodes) {
    const parent = node.parent;

    if (parent && 'name' in parent) {
      const { name } = parent;
      createPage({
        path: name,
        component,
        context: {
          site: data.site,
          name,
          prefixed: node.prefixed,
          elements: node.elements
        }
      })
    } else {
      console.error('TEI file is null or does not have a name property');
    }
  }
}

async function makeIntroduction({createPage, reporter, graphql}: IMakePages) {
  const component = path.resolve(`./src/templates/introduction.tsx`)

  const result = await graphql(`
    query Intro {
      allMarkdownRemark {
        edges {
          node {
            html
            frontmatter {
              path
              title
            }
          }
        }
      }
      site {
        siteMetadata {
          title
          htmlTitle
          issue {
            full
            short
            date
          }
          doi
          authors {
            first
            middle
            last
            affiliations
            orcid
          }
          issn
          keywords
          description
        }
      }
      orcid: allFile(filter: {relativePath: {eq: "orcid.png"}}) {
        nodes {
          childImageSharp {
            gatsbyImageData(width: 16)
          }
        }
      }
    }
  `)
  if (result.errors) {
    reporter.panicOnBuild(`Error while running GraphQL query.`)
    return
  }

  const data = result.data as Queries.IntroQuery

  data.allMarkdownRemark.edges.forEach(({ node }) => {
    createPage({
      path: node.frontmatter?.path!,
      component,
      context: {
        site: data.site,
        slug: slugify(data.site?.siteMetadata?.title || ""),
        doi: data.site?.siteMetadata?.doi,
        orcid: data.orcid,
        html: node.html,
        title: node.frontmatter?.title
      }
    })   
  })
}
