import { createTheme } from "@mui/material/styles"

// Extend colors and allow extensions on Button

declare module '@mui/material/styles' {
  interface Palette {
    default: Palette['primary'];
  }
  interface PaletteOptions {
    default: PaletteOptions['primary'];
  }
}

declare module '@mui/material/Button' {
  interface ButtonPropsColorOverrides {
    default: true;
  }
}

export const mainColor = "#dc3522"
const secondaryColor = "#fbebda"

// A custom theme for CETEIcean
// It is not intended to be comprehensive; add further rules as needed.
const theme = createTheme({
  typography: {
    fontFamily: "EB Garamond, Serif",
    body1: {
      fontSize: "1.25rem",
      paddingBottom: "1.25rem",
    },
    body2: {
      fontSize: "1rem"
    },
    subtitle1: {
      fontSize: "1.4rem",
    }
  },
  components: {
    MuiCssBaseline: {
      styleOverrides: {
        "@font-face": [
          {
            fontFamily: "EB Garamond",
            fontStyle: "normal",
            fontDisplay: "swap",
            fontWeight: 400,
            fontSize: "1.25rem",
          },
        ],
        "::selection": {
          background: mainColor,
          color: secondaryColor
        },
        "a, a:visited, a:hover, a:active": {
          color: mainColor,
        },
        "h1, h2, h3, h4, h5, h6": {
          color: "#333",
        },
        "tei-reg": {
          display: "inline"
        },
        ".orig tei-reg": {
          display: "none"
        },
        "*[rend=transcription_only]": {
          display: "none"
        },
        "*[rend~=underlined]": {
          textDecoration: "underline"
        },
        ".orig *[rend=transcription_only]": {
          display: "block"
        },
        "tei-orig": {
          display: "none"
        },
        ".orig tei-orig": {
          display: "inline"
        },
        "tei-ab": {
          display: "block",
          marginTop: "1.25em",
          marginBottom: "1.25em",
        },
        "tei-emph": {
          fontStyle: "italic"
        },
        "tei-head": {
          display: "block",
          fontSize: "180%",
        },
        "tei-lb:after": {
          content: "'\\a'",
          whiteSpace: "pre"
        },
        "tei-p": {
          display: "block",
          marginTop: "1.25em",
          marginBottom: "1.25em",
          textAlign: "justify"
        },
        "tei-q:before": {
          content: `"“"`
        },
        "tei-q:after": {
          content: `"”"`
        },
        "tei-ptr": {
          margin: "0 0.2em 0 0.2em",
          verticalAlign: "super",
          lineHeight: "1em"
        },
        "tei-item": {
          display: "block"
        },
        "tei-div": {
          display: "block",
          marginBottom: "1em"
        },
        "tei-persName[ref]": { // Generate list of refs that should be rendered as annotations
          borderBottom: `2px solid ${mainColor}`,
          cursor: "pointer"
        },
        "tei-div[type=comment]": {
          padding: "1em",
          border: `1px solid ${mainColor}`,
          fontStyle: "italic"
        },
        "tei-salute": {
          display: "block",
          marginTop: "1em"
        },
        "tei-dateline": {
          display: "block",
        },
        "tei-signed": {
          display: "block",
        },
        "tei-del": {
          textDecoration: "line-through"
        },
        ".orig *[rend*=orig_inline]": {
          display: "inline"
        },
        ".orig *[rend*=orig_mt-0]": {
          marginTop: 0
        },
        ".orig *[rend*=orig_mb-0]": {
          marginBottom: 0
        },
        "*[rend*=mt-1]": {
          marginTop: "1em"
        },
        "tei-tei tei-teiHeader": {
          display: "none"
        },
        "tei-tei:not(:has(tei-tei)) tei-teiHeader": {
          display: "block",
          padding: "1em",
          border: `1px solid ${mainColor}`
        },
        "tei-fileDesc > *": {
          display: "none"
        },
        "tei-fileDesc > tei-titleStmt": {
          display: "block",          
        },
        "tei-fileDesc > tei-titleStmt > *, tei-correspDesc": {
          display: "none"
        },
        "tei-abstract": {
          fontStyle: "italic"
        },
        "tei-fileDesc > tei-titleStmt > tei-title": {
          display: "block",
          fontSize: "180%",
          fontStyle: "normal"
        },
        "tei-fileDesc > tei-titleStmt > tei-title[type=sub]": {
          fontSize: "120%",
        },
        "tei-seg[rend=lineend]": {
          display: "block",
          textAlign: "right"
        },
        "tei-hi[rend~=sup]": {
          verticalAlign: "super",
          fontSize: "smaller",
        }
      }
    },
  },
  palette: {
    default: {
      main: "#444",
    },
    text: {
      primary: "#444",
    },
    primary: {
      main: mainColor,
    },    
    secondary: {
      main: secondaryColor,
    },
    background: {
      default: "#fff",
    },
  },
  breakpoints: {
    values: {
      xs: 0,
      sm: 600,
      md: 960,
      lg: 1280,
      xl: 1536,
    }
  }
})

export default theme
