---
path: "/"
---

**This is a template for the Micro-Edition subsites for the _Scholarly Editing_ journal.** Typically this page would show the _Introduction_ to the Micro-Edition. In this case, this page provides some guidance over common Micro-Edition features included in this template. All the features are optional and should be pared down depending on the needs of each Micro-Edition.

## Facsimile Pages

...

## Footnotes

This guide will walk you through using the ptr and note components to create reusable, color-coded footnotes within your TEI-based XML project. Each footnote consists of two main parts: a clickable ptr component, which triggers the display of the footnote, and a corresponding note component that contains the footnote’s content.

Component Breakdown

1. ptr Component

Purpose: The ptr component acts as a button or trigger that the user clicks to reveal a footnote. It links to a specific note using the target attribute.

Usage: The ptr component must specify the target footnote using an identifier (target), which corresponds to the xml:id attribute in the note component.

Example: 
                    `<p> Second footnote (person) <ptr target="#fn2" n="2"/></p>`
					
Attributes:

	• target: The unique identifier linking to the corresponding note. In this case, target="#fn2" connects to the footnote with xml:id="fn2".
	• n: The footnote number, which helps order and display footnotes systematically.

2. note Component

Purpose: The note component holds the content of the footnote. It is identified using the xml:id attribute, which the ptr component references.

Attributes:




	• "xml:id": The unique identifier that links to the ptr component. It must match the target attribute in the ptr component.
	• "type" (optional): Defines the category of the note, which maps to a specific color for both the ptr and note.
	• Example:
                    `<note xml:id="fn2" type="person">Person note</note>`

Color Customization with noteTypeColors

The color of the ptr and note is determined by the type attribute defined in the note. The colors are mapped in the ceteicean.tsx file, allowing you to customize the visual appearance of each note type.



1. Defining Note Types and Colors

A color scheme is defined as follows in Ceteician.tsx:

	const noteTypeColors: IColors = {
		"default": "#000000",  // Black
		"person": "#0000FF",   // Blue
		"source": "#008000",   // Green
		"critical": "#FF0000", // Red
		"editorial": "#FFA500" // Orange
		};


    •	"default": Used when no type is specified. The color is black.
	•	"person": Notes related to individuals are shown in blue.
	•	"source": Source-related notes are shown in green.
	•	"critical": Critical commentary notes are shown in red.
	•	"editorial": Editorial notes are shown in orange.

This type to color mapping can be changed as neccessary by assigning types to the hex color codes in Ceteician.tsx.

To use a specific color, set the type attribute in the note component: `<note xml:id="fn3" type="source">Source note</note>`

	•	The ptr and note will both be colored according to the type, in this case, green for “source.”

2. Default Color Mapping

    The mapping of the default hex color code can be changed in Ceteician.tsx in instances where the default colors of the ptr and note are the same default color.

    However, if there is a need to set the default colors of the ptr and note to be different this can be done in their respective files Ptr.tsx and Note.tsx. 

    For example if you want notes without a type passed to revert to a person type (color: Blue) but wanted the ptr color to stay as black, you could update "default" in this part of Note.tsx to "person".

TEI Component Routes in Ceteicean

The Ceteicean component is central to rendering TEI-based content in a structured and styled manner. To handle specific TEI elements, routes are defined, mapping TEI tags to their respective React components. Here’s a detailed breakdown of how the TEI component routes work:

TEI Component Routes

	1.	Setting Up Routes
The routes object in EditionCeteicean specifies how different TEI elements are rendered. Each key represents a TEI element, and the value is the React component responsible for displaying that element. This setup allows you to customize the appearance and behavior of TEI elements.

	const routes: Routes = {
		"tei-tei": Tei,
		"tei-teiheader": TeiHeader,
		"tei-pb": (props) => <Pb facs={facs} {...props} />,
		"tei-ptr": (props) => <Ptr {...props} colorMap={noteTypeColors} />, // Pass colorMap
		"tei-note": (props) => <Note {...props} colorMap={noteTypeColors} /> // Pass colorMap
		};

	2.	Explanation of Route Mappings

	•	"tei-ptr":
	Customization: The Ptr component receives colorMap to apply color coding based on note types.
	Purpose: Acts as a pointer or reference, linking to notes defined elsewhere in the TEI content.

	•	"tei-note":
	Customization: The Note component is also given colorMap for consistent color-coded rendering of note types.
	Purpose: Displays <note> elements, applying color coding based on their type attribute.

## Shared Context for Display Options

...

### Context for multilingual applications

...

## Sticky Micro-Edition AppBar

...

## Accessible Drop down menu with display options

...

## Table of Contents