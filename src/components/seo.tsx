/**
 * SEO component that queries for data with
 *  Gatsby's useStaticQuery React hook
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */

import React from "react"
import { useStaticQuery, graphql } from "gatsby"

interface Props {
  children: JSX.Element | JSX.Element[]
}

interface Author {
  first: string
  middle?: string
  last: string,
  affiliations: string[]
  orcid?: string
}

function SEO({ children }: Props) {
  const { site } = useStaticQuery(
    graphql`
      query {
        site {
          siteMetadata {
            title
            description
            authors {
              first
              middle
              last
            }
          }
        }
      }
    `
  )

  const metaDescription = site.siteMetadata.description

  return (
    <>
      <meta name="description" content={metaDescription}/>
      <meta name="og:description" content={metaDescription}/>
      <meta name="twitter:description" content={metaDescription}/>
      <meta name="twitter:creator" content={site.siteMetadata.authors.map((a: Author) => `${a.first} ${a.middle || ''} ${a.last}`).join(', ')} />
      <link
        href="https://fonts.googleapis.com/css2?family=EB+Garamond:ital,wght@0,400;0,500;1,400;1,500&display=swap"
        rel="stylesheet"/>
      {children}
    </>
  )
}

export default SEO
