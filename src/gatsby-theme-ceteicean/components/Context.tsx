// Context.tsx
import React from "react"

export interface IOptions { [key: string]: string | boolean}

export interface IColors {
  default: string;
  person: string;
  source: string;
  critical: string;
  editorial: string;
  [key: string]: string;
}

export const Colors: IColors = {
  default: "#000000",
  person: "#0000FF",
  source: "#008000",
  critical: "#FF0000",
  editorial: "#FFA500"
};

export type ContextType = {
  contextOpts: IOptions
  setContextOpts: React.Dispatch<React.SetStateAction<IOptions>>
}

export const DisplayContext = React.createContext<ContextType>({
  contextOpts: {},
  setContextOpts: () => console.warn('no DisplayContext options provider')
})

export type TNote = {
  id: string
  n: number
  type?: string
}

type NoteContextType = {
  note: TNote | null
  setNote: React.Dispatch<React.SetStateAction<TNote | null>>
}

export const NoteContext = React.createContext<NoteContextType>({
  note: null,
  setNote: () => console.warn('no note data provider')
})

export type TEntity = {
  id: string
  type?: string
}

type EntityContextType = {
  entity: TEntity | null
  setEntity: React.Dispatch<React.SetStateAction<TEntity | null>>
}

export const EntityContext = React.createContext<EntityContextType>({
  entity: null,
  setEntity: () => console.warn('no entity data provider')
})