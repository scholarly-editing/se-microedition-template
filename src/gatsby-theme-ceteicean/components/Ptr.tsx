import React from "react";
import { TBehavior, SafeUnchangedNode } from "gatsby-theme-ceteicean/src/components/DefaultBehaviors";
import { Behavior } from "gatsby-theme-ceteicean/src/components/Behavior";
import { NoteContext, EntityContext, TNote, Colors } from "./Context";
import styled from '@emotion/styled';
import { IColors } from "../../displayoptions";

type TEIProps = {
  teiNode: Node;
  availableRoutes?: string[];
  colorMap: IColors;
};

const Footnote = styled.a<{ $typeColor: string }>`
  display: inline-block;
  font-weight: 500;
  font-size: 0.75rem;
  min-width: 20px;
  height: 20px;
  border-radius: 10px;
  color: #fff;
  background-color: ${(props) => props.$typeColor};
  text-align: center;
  text-indent: 0;
  cursor: pointer;
  word-break: normal;

  &:hover {
    color: #fff;
    text-decoration: none;
  }
`;

const defaultColorMap = {
  "default": "#000000",  
  "person": "#0000FF",  
  "source": "#008000",   
  "critical": "#FF0000", 
  "editorial": "#FFA500", 
}

export type PtrBehavior = (props: TEIProps) => JSX.Element | null


const Ptr: PtrBehavior = (props: TEIProps) => {
  const { setNote } = React.useContext(NoteContext);
  const { setEntity } = React.useContext(EntityContext);
  
  // Use provided colorMap or defaultColorMap
  const colorMap = props.colorMap || defaultColorMap;

  const el = props.teiNode as Element;
  const n = el.getAttribute('n');
  const target = el.getAttribute('target');
  const id = target?.replace('#', '') || '';
  const note = el.ownerDocument.getElementById(id);

  // Get the note type and corresponding color
  const type = note?.getAttribute('type') || 'default';
  const typeColor = colorMap[type] || colorMap['default'];  // Ensure a color is always found

  if (n && target) {
    const noteData: TNote = {
      id,
      n: parseInt(n),
      type
    };

    const handleDialog = (e: React.KeyboardEvent | React.MouseEvent) => {
      if (e.type !== 'keydown' || (e.type === 'keydown' && (e as React.KeyboardEvent).key === 'Enter')) {
        setEntity(null);
        setNote(noteData);
      }
    };

    return (
      <Behavior node={props.teiNode} key={n}>
        <Footnote 
          $typeColor={typeColor} 
          onClick={handleDialog} 
          onKeyDown={handleDialog} 
          tabIndex={0}
        >
          {n}
        </Footnote>
      </Behavior>
    );
  }
  return <SafeUnchangedNode {...props} />;
};

export default Ptr;