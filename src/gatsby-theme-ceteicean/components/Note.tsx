import React from "react"

import { Behavior } from "gatsby-theme-ceteicean/src/components/Behavior"

import { NoteContext } from "./Context"

import theme from '../../theme'
import { TransitionProps } from "@mui/material/transitions/transition"
import Slide from "@mui/material/Slide"
import useMediaQuery from "@mui/material/useMediaQuery"
import IconButton from "@mui/material/IconButton"
import CloseIcon from '@mui/icons-material/Close';
import Dialog from "@mui/material/Dialog"
import DialogTitle from "@mui/material/DialogTitle"
import Typography from "@mui/material/Typography"
import DialogContent from "@mui/material/DialogContent"
import DialogContentText from "@mui/material/DialogContentText"
import Card from "@mui/material/Card"
import CardHeader from "@mui/material/CardHeader"
import CardContent from "@mui/material/CardContent"
import { SafeUnchangedNode } from "gatsby-theme-ceteicean/src/components/DefaultBehaviors"
import Chip from "@mui/material/Chip"

type TEIProps = {
  teiNode: Node,
  availableRoutes?: string[],
  colorMap?: Record<string, string>
}

const defaultColorMap = {
  "default": "#000000",  
  "person": "#0000FF",  
  "source": "#008000",   
  "critical": "#FF0000", 
  "editorial": "#FFA500", 
}

const Transition = React.forwardRef(function Transition(
  props: TransitionProps & {
    children: React.ReactElement<any, any>;
  },
  ref: React.Ref<unknown>,
) {
  return <Slide direction="up" ref={ref} {...props} />
});

export type NoteBehavior = (props: TEIProps) => JSX.Element | null

const Note: NoteBehavior = (props: TEIProps) => {
  const { note, setNote } = React.useContext(NoteContext);
  const [cardPosition, setCardPosition] = React.useState(350);

  const colorMap = props.colorMap || defaultColorMap;

  const isScreenSmall = useMediaQuery(theme.breakpoints.down('lg'));

  React.useEffect(() => {
    const fromTop = document.documentElement.scrollTop > 150 ? document.documentElement.scrollTop + 150 : document.body.scrollTop;
    setCardPosition(fromTop > 0 ? fromTop : 350);
  }, [note]);

  const el = props.teiNode as Element;
  const id = el.getAttribute('id');

  if (note) {
    const noteType = el.getAttribute('type') || 'default';
    const noteColor = colorMap[noteType.toLowerCase()] || colorMap['default'];

    const noteTypeC = noteType !== 'default' ? (
      <>
        <br/><Chip 
          size="small" 
          label={noteType} 
          sx={{
            color: "#fff", 
            backgroundColor: noteColor
          }}
        />
      </>
    ) : null;

    if (note.id === id) {
      let content: JSX.Element | undefined = undefined;
      const closeNote = (
        <IconButton aria-label="close note" onClick={() => setNote(null)}>
          <CloseIcon />
        </IconButton>
      );

      if (isScreenSmall) {
        content = (
          <Dialog
            open={note.id === id}
            scroll="body"
            TransitionComponent={Transition}
            keepMounted
            onClose={() => setNote(null)}
            aria-labelledby="alert-dialog-slide-title"
            aria-describedby="alert-dialog-slide-description"
          >
            <DialogTitle 
              id="alert-dialog-slide-title" 
              sx={{
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
                color: noteColor
              }}
            >
              <Typography variant="h6">Note {note.n}{noteTypeC}</Typography>              
              <IconButton aria-label="close note" onClick={() => setNote(null)}>
                <CloseIcon />
              </IconButton>
            </DialogTitle>
            <DialogContent>
              <DialogContentText component="div" id="alert-dialog-slide-description">
                <SafeUnchangedNode {...props} />
              </DialogContentText>
            </DialogContent>
          </Dialog>
        );
      } else {
        content = (
          <Card
            style={{ top: cardPosition }}
            sx={{
              maxWidth: "300px",
              position: "absolute",
              right: "1.5rem",
              overflowWrap: "break-word",
            }}
          >
            <CardHeader
              action={closeNote}
              title={
                <Typography variant="h6" sx={{ color: noteColor }}>
                  Note {note.n}{noteTypeC}
                </Typography>
              }
            />
            <CardContent>
              <SafeUnchangedNode {...props} />
            </CardContent>
          </Card>
        );
      }

      return (
        <Behavior node={props.teiNode}>
          {content}
        </Behavior>
      );
    }
  }
  return null;
};

export default Note