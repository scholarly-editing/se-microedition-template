import React from "react"
import { IGatsbyImageData } from "gatsby-plugin-image"
import { graphql, useStaticQuery } from "gatsby"
import Ceteicean, { Routes } from "gatsby-theme-ceteicean/src/components/Ceteicean"
import { Tei, TeiHeader } from "gatsby-theme-ceteicean/src/components/DefaultBehaviors"
import Pb from "./Pb"
import Layout from "../../components/layout"
import Container from "@mui/material/Container"
import SEO from "../../components/seo"
import Ptr from "./Ptr"
import Note from "./Note"
import { DisplayContext, EntityContext, IOptions, NoteContext, TEntity, TNote } from './Context'
import { IColors } from "../../displayoptions"

interface Props {
  pageContext: {
    name: string
    prefixed: string
    elements: string[]
    site: Site
  },
  location: string
}

export interface Fac {
  name: string
  childImageSharp: {
    gatsbyImageData: IGatsbyImageData
  }
}

const noteTypeColors:IColors = {
  "default": "#000000",  
  "person": "#0000FF",  
  "source": "#008000",   
  "critical": "#FF0000", 
  "editorial": "#FFA500", 
}

const EditionCeteicean = ({ pageContext }: Props) => {
  const queryData = useStaticQuery(graphql`
    query general {
      facs: allFile(filter: {relativeDirectory: {in: "facs"}}) {
        nodes {
          name
          childImageSharp {
            gatsbyImageData
          }
        }
      }
    }
  `)
  const facs: Fac[] = queryData.facs.nodes

  const routes: Routes = {
    "tei-tei": Tei,
    "tei-teiheader": TeiHeader,
    "tei-pb": (props) => <Pb facs={facs} {...props} />,
    "tei-ptr": (props) => <Ptr {...props} colorMap={noteTypeColors} />, // Pass colorMap
    "tei-note": (props) => <Note {...props} colorMap={noteTypeColors} /> // Pass colorMap
  };

  const [displayOpts, setDisplayOpts] = React.useState<IOptions>({ originalText: true })
  const [note, setNote] = React.useState<TNote | null>(null)
  const [entity, setEntity] = React.useState<TEntity | null>(null) 

  return (
    <DisplayContext.Provider value={{ contextOpts: displayOpts, setContextOpts: setDisplayOpts }}>
      <EntityContext.Provider value={{ entity, setEntity }}>
        <NoteContext.Provider value={{ note, setNote }}>
          <Layout location="Edition" editionPage={true}>
            <Container component="main" maxWidth="md">
              <Ceteicean pageContext={pageContext} routes={routes} />
            </Container>
          </Layout>
        </NoteContext.Provider>
      </EntityContext.Provider>
    </DisplayContext.Provider>
  )
}

export default EditionCeteicean

export const Head = ({ pageContext }: Props) => {
  const { site } = pageContext
  const { htmlTitle, issue } = site.siteMetadata
  const safeTitle = htmlTitle.replace(/<[^>]+>/g, '') || ""
  const fullTitle = `${safeTitle} | ${issue.short} | Scholarly Editing`
  return (
    <SEO>
      <html lang="en" />
      <title>{fullTitle}</title>
    </SEO>
  )
}