import { mainColor } from "./theme"

export interface IColors {
  default: string;
  person: string;
  source: string;
  critical: string;
  editorial: string;
  [key: string]: string;
}

export const Colors: IColors = {
  "default": "#000000",  
  "person": "#0000FF",  
  "source": "#008000",   
  "critical": "#FF0000", 
  "editorial": "#FFA500", 
};

